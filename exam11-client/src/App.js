import React, {Component} from 'react';
import {Route, Switch} from "react-router-dom";

import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Layout from "./containers/Layout/Layout";
import NewItem from "./containers/NewItem/NewItem";
import AllItems from "./containers/AllItems/AllItems";

class App extends Component {
    render() {
        return (
            <Layout>
                <Switch>
                    <Route path="/" exact component={AllItems} />
                    <Route path="/items/new" exact component={NewItem} />
                    <Route path="/register" exact component={Register} />
                    <Route path="/login" exact component={Login} />
                </Switch>
            </Layout>
        )
    }
}

export default App;
