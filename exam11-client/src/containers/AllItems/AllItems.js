import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import AllList from "../../components/AllList/AllList";
import {fetchItems} from "../../store/action/allItems";

class AllItems extends Component {
    componentDidMount() {
        this.props.onFetchItems();
    }

    render() {
        return (
            <Fragment>
                <PageHeader>
                    All items
                </PageHeader>

                {this.props.items.map(item => (
                    <AllList
                        key={item._id}
                        id={item._id}
                        title={item.title}
                        description={item.description}
                        price={item.price}
                        image={item.image}
                        user={item.user}
                    />
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        items: state.items.items,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchItems: () => dispatch(fetchItems())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AllItems);