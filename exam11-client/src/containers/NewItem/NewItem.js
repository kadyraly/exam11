import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";
import {createItem} from "../../store/action/allItems";
import ItemForm from "../../components/ItemForm/ItemForm";


class NewItem extends Component {
    createItem = (itemData, token) => {
        this.props.onItemCreated(itemData, token).then(() => {
            this.props.history.push('/');
        });
    };

    render() {
        return (
            <Fragment>
                <PageHeader>New item</PageHeader>
                <ItemForm onSubmit={this.createItem} />
            </Fragment>
        );
    }
}
const mapStateToProps = state => {
    return {
        user: state.users.user
    }

};

const mapDispatchToProps = dispatch => {
    return {
        onItemCreated: (itemData, token) => {
            return dispatch(createItem(itemData, token))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(NewItem);