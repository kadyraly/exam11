import axios from '../../axios-api';
import {CREATE_ITEM_SUCCESS, FETCH_ITEMS_SUCCESS} from "./actionTypes";



export const fetchItemsSuccess = items => {
    return {type: FETCH_ITEMS_SUCCESS, items};
};

export const fetchItems = () => {
    return dispatch => {
        axios.get('/allItems').then(
            response => dispatch(fetchItemsSuccess(response.data))
        );
    }
};

export const createItemSuccess = (items) => {
    return {type: CREATE_ITEM_SUCCESS, items};
};

export const createItem = (itemData)=> {
    return (dispatch, getState) => {
        return axios.post('/allItems', itemData, {headers: {'Token': getState().users.user.token}}).then(
            response => dispatch(createItemSuccess())
        );
    };
};