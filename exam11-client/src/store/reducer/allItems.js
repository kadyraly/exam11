
import {FETCH_ITEMS_SUCCESS} from "../action/actionTypes";

const initialState = {
    items: []
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_ITEMS_SUCCESS:
            return {...state, items: action.items};
        default:
            return state;
    }
};

export default reducer;