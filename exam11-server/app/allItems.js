const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const AllItems = require('../model/AllItems');
const User = require('../model/User');

const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

const createRouter = () => {

    router.get('/', async (req, res) => {
        AllItems.find().populate('user', 'category')
            .then(results => res.send(results))
            .catch(() => res.sendStatus(500));
    });

    router.post('/', upload.single('image'), async (req, res) => {
        const itemData = req.body;
        if (req.file) {
            itemData.image = req.file.filename;
        } else {
            itemData.image = null;
        }
        const tokenId = req.get('Token');
        const user = await User.findOne({token: tokenId}).select('_id');
        if (user) {
            itemData.user = user._id;
            const allItem = new AllItems(itemData);
            await allItem.save();
            res.send({allItem});

        } else {
            res.sendStatus(401)
        }
    });
    router.get('/:id', (req, res) => {
        const id = req.params.id;
        db.collection('allItems')
            .findOne({_id: new ObjectId(req.params.id)})
            .then(result => {
                if (result) res.send(result);
                else res.sendStatus(404);
            })
            .catch(() => res.sendStatus(500));
    });



    return router;
};

module.exports = createRouter;