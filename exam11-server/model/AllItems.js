const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AllItemsSchema = new Schema({

    title: {
        type: String, required: true
    },
    price: {
        type: Number, required: true
    },
    description: String,
    image: String,
    category: {
        type: Schema.Types.ObjectId,
        ref: 'Category'

    },
    user: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },

});

const AllItem = mongoose.model('AllItems', AllItemsSchema);

module.exports = AllItem;